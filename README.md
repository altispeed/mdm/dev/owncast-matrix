# Owncast/Matrix Chat Bridge

This bot bridges a matrix chat room and your owncast chat together. This bot uses a normal matrix account so you don't need your own home server to use this bot.

## Requirements to run

* An owncast server
* Somewhere to run the bridge/bot. Needs to be routable for your owncast server. Can run on the same server as your owncast server.
* An account on a matrix server.
* A matrix room to bridge.

## Steps to setup

### Bot setup

Clone this repo.
```bash
git clone https://gitlab.com/sleuth56/owncast-matrix
```
Change the names of the default config files.
```
cd owncast-matrix
mv docker-compose.template docker-compose
mv configuration.yaml.template configuration.yaml
```

### Matrix credentials

Create a matrix account on a matrix server.
Put the username and password for the bot's account into the configuration.yaml file.
Create your matrix room. (Or open the room you want to bridge)
In Element with the room open. Click on the room name in the top.
Select settings.
Advanced.
Copy the internal room id. It will look like this `!AAAAAAAAAAAAAAAAAA:example.com`
Paste it into the main room part of the configuration.yaml file. Replacing the example room id.

#### Homeserver url

In matrix the domain that you go to might not be the actual server address. The best way to get the actual server address is with this curl command. Replace `matrix.org` with your home server's domain name.
```bash
curl https://matrix.org/.well-known/matrix/client
```
```json
{
    "m.homeserver": {
        "base_url": "https://matrix-client.matrix.org" <<<<< Copy the URL in the quotes.
    },
    "m.identity_server": {
        "base_url": "https://vector.im"
    },
    "org.matrix.msc3575.proxy": {
        "url": "https://slidingsync.lab.matrix.org"
    }
}
```
Paste the URL you coped from the curl command into the homeserver section in the configuration.yaml file.

#### Encryption

[Encryption guide](https://sleuth56.gitlab.io/Matrix-Community-Manager/Manual-Installation/#1-using-curl-to-login-to-the-bots-matrix-account)

### Owncast

Open your owncast admin pannel `https://YOUR_OWNCAST_INSTANCE/admin` 
Open the integration tab.
Click on webhooks.
Create webhooks, for the URL put `http://IP_OR_DOMAIN_FOR_ThIS_BOT/skill/owncast-matrix/message` tick the box for `When a user sends a chat message` and hit OK.

Open the access token section of the admin panel.
Create access token. Put the name you want your bot to show up as in the first field. Tick the `Can send chat messages on behalf of the owner of this token.` and hit OK.

Copy the access token and paste it into the `auth_token` section of the configuration file.

Put the URL to your owncast instance in `server_address`.

Run `docker-compose up` to start the bot. Add a `-d` to that command to run detached from your terminal after you test that your bot is working.

### HTTPS

I would recommend placing the bot behind a HTTPS proxy to encrypt all your traffic. There isn't anything fancy here. Setup your proxy and change all the URLs on owncast and the bot to use https and remove the port from the URLs. 