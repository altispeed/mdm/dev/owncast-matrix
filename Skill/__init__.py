""" Imports for Opsdroid """
from opsdroid.connector.matrix import ConnectorMatrix
from opsdroid.skill import Skill
from opsdroid.events import (
  OpsdroidStarted, 
  Message, 
  Reaction
  )
from opsdroid.matchers import (
  match_event, 
  match_regex, 
  match_webhook, 
  match_catchall
  )

""" Other imports """
import emoji
import requests
from aiohttp.web import (
  Request, 
  Response
  )

""" Setup logging """
import logging
_LOGGER = logging.getLogger(__name__)

class Owncast_Matrix(Skill):

  @match_webhook('message')
  async def owncats_message_handler(self, event: Request):
    data = await event.json()
    _LOGGER.info(data)
    await self.opsdroid.send(Message(
        f"{data['eventData']['user']['displayName']}: {data['eventData']['body']}", 
        target=self.config.get('main'))
        )

  @match_catchall(messages_only=True)
  async def matrix_message_handler(self, opsdroid, config, message):
    _LOGGER.info(message)
    body = {"body": f"{message.user}: {message.text}"}
    header = {'Content-Type': 'application/json', 'Authorization': f"Bearer {self.config['auth_token']}"}
    raw_resp = requests.post(f"{self.config['server_address']}/api/integrations/chat/send", headers=header, json=body)
