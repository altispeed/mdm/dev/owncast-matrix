# Owncast API Reference

https://owncast.online/thirdparty/webhooks/

## Chat message webhook payload

```json
{
  "type": "CHAT",
  "eventData": {
    "user": {
      "id": "Za9XsQx4R",
      "displayName": "cranky-leavitt",
      "displayColor": 214,
      "createdAt": "2023-03-04T18:54:35.107773843Z",
      "previousNames": [
        "cranky-leavitt"
      ],
      "nameChangedAt": "0001-01-01T00:00:00Z",
      "isBot": false,
      "authenticated": false
    },
    "clientId": 4,
    "body": "test",
    "rawBody": "test",
    "id": "EcdWrwx4g",
    "visible": true,
    "timestamp": "2023-03-04T20:08:32.918363277Z"
  }
}
```

## Curl command to send a chat message

```bash
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer _Yagpw-wI_wZ1KEbsBdvNtXA42efleFXWYF9Jv7_LAM=" -d '{"body": "Hello world!"}' http://live.rexrobotics.org:8080/api/integrations/chat/send
```